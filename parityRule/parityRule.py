#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Universite de Geneve, Switzerland
# E-mail contact: sebastien.leclaire@etu.unige.ch
#
# The Parity Rule
#

from numpy import *
import matplotlib.pyplot as plt
from matplotlib import cm
import scipy
import copy
from scipy.misc import imread
    
# Definition of functions
def readImage(string): # This function only work for monochrome BMP. 
    image =  imread(string,1);
    image[image == 255] = 1
    image = image.astype(int) 
    return image # Note that the image output is a numPy array of type "int".

# Main Program

# Program input, i.e. the name of the image "imageName" and the maximum number of iteration "maxIter"
imageName = 'image3.bmp'
maxIter   = 32

# Read the image and store it in the array "image"
image = readImage(imageName) # Note that "image" is a numPy array of type "int".
# Its element are obtained as image[i,j]
# Also, in the array "image" a white pixel correspond to an entry of 1 and a black pixel to an entry of 0.

# Get the shape of the image , i.e. the number of pixels horizontally and vertically. 
# Note that the function shape return a type "tuple" (vertical_size,horizontal_size)
imageSize = shape(image);

# Print to screen the initial image.
print('Initial image:')
plt.clf()
plt.imshow(image, cmap=cm.gray)
plt.show()
plt.pause(0.1)

# Main loop
for it in range(1,maxIter+1):
    
    imageCopy = copy.copy(image);
    
    # You need to write here the core of the parity rule algorithm.
    # Altough not mandatory, you might need to use the array "imageCopy" and the tuple "imageSize".
    
    # Loop through all pixels of the image
    nx = imageSize[0]
    ny = imageSize[1]
    for kx in range(nx):
        for ky in range(ny):
            
            thisSum = 0
            # Compute the sum, use periodic boundaries
            top = imageCopy[kx,ky-1] if ky > 0 else imageCopy[kx,-1]
            left = imageCopy[kx-1,ky] if kx > 0 else imageCopy[-1,ky]
            right = imageCopy[kx+1,ky] if kx < nx-1 else imageCopy[0,ky]
            bottom = imageCopy[kx,ky+1] if ky < ny-1 else imageCopy[kx,0]
            
            thisSum += top+left+right+bottom
            
            # Modify the pixel in the original image
            image[kx,ky] = 0 if thisSum%2==0 else 1
    
    # Print to screen the image after each iteration.
    print('Image after',it,'iterations:')
    plt.clf()
    plt.imshow(image, cmap=cm.gray)
    plt.show()
    plt.pause(0.1)
        
# Print to screen the number of white pixels in the final image
print("The number of white pixels after",it,"iterations is: ", sum(image))
